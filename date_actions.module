<?php

/**
 * @file General data related rules integration
 *
 * Provides rules action info and action callback for formatting dates.
 */

/**
 * Implements hook_rules_action_info().
 */
function date_actions_rules_action_info() {

  $items = array();
  $items['date_actions_format_date'] = array(
    'label' => t('Format date'),
    'group' => t('Data'),
    'parameter' => array(
      'data' => array(
        'type' => 'date',
        'label' => t('Date'),
        'description' => t('Specifies the date to be modified using a data selector, e.g. "site:current-date".'),
        'restriction' => 'selector',
        'allow null' => FALSE,
      ),
      'format' => array(
         // @todo Change this to a text field, not a textarea, if possible.
        'type' => 'text',
        'label' => t('Format'),
        'description' => t('The format of the return value. See format_date() for more info.'),
        'restriction' => 'input',
        'allow null' => FALSE,
        'optional' => FALSE,
      ),
      'value' => array(
        'type' => '*',
        'label' => t('Value'),
        'description' => t('The new value to set for the specified date.'),
        'allow null' => FALSE,
        'optional' => FALSE,
      ),
    ),
  );
  return $items;

}

/**
 * Action callback to format a timestamp.
 */
function date_actions_date($data, $format, $value, $settings, $state, $element) {

  $value = format_date($data, 'custom', $format);
  return array('value' => $value);

}
